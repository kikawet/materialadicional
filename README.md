[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS[^nota1]
## Adicional: Semáforos y secciones `synchronized` en Java

Una de las situaciones más comunes en la programación concurrente es cuando los hilos deben compartir recursos. En las aplicaciones concurrentes, es común que diferentes hilos accedan a las mismas variables o accedan al mismo archivo o tengan acceso a la misma base de datos. El acceso concurrente podría provocar inconsistencia en los datos y debemos tener mecanismos a nuestra disposición que eviten estos errores. En las clases de teoría de la asignatura trataremos este problema, que se conoce como problema de **sección crítica**, con más detalle. De forma resumida, una **sección crítica** es un bloque de código que tienen acceso a recursos compartidos entre diferentes hilos y que sólo podrá ser ejecutarlo por uno de ellos a la vez.

Java nos proporciona los elementos necesarios para sincronizar los hilos. Por medio de estos elementos de sincronización se puede garantizar que sólo un hilo esté ejecutando el código comprendido en una **sección crítica**. De esta forma, el resto de hilos que intenten acceder a la sección crítica suspenderán su ejecución mientras esté ya un hilo ejecutando esa **sección crítica**.

El mecanismo básicos de sincronización que nos proporciona Java es: la palabra reservada `synchronized`. A continuación se presentarán unos ejemplos donde se muestran las diferentes formas de uso de la palabra reservada `synchronized` en Java:

1. [Sincronizando un método.](https://gitlab.com/ssccdd/materialadicional/-/blob/master/Ejemplo1.md)
2. [Organización de atributos independientes en clases sincronizadas.](https://gitlab.com/ssccdd/materialadicional/-/blob/master/Ejemplo2.md)
3. [Usando condiciones en un código sincronizado.](https://gitlab.com/ssccdd/materialadicional/-/blob/master/Ejemplo3.md)

En los ejemplos anteriores, aprendimos los conceptos de **sincronización** y **sección crítica**. Básicamente, se presentaba la **sincronización** cuando una o más tareas concurrentes comparten un recurso, por ejemplo, un objeto o un atributo de un objeto. Al conjunto de sentencias que permiten el acceso a ese recurso compartido, y que solo puede ejecurar un único hilo, se definió como la **sección crítica**.

Si no utilizamos los elementos apropiados, podemos encontrarnos con resultados erróneos, inconsistencia en los datos, etc … Por tanto, se han utilizado los primeros mecanismos que nos proporciona Java para evitar esos problemas y garantizar el acceso a la sección crítica. Ahora utilizaremos mecanismos de **alto nivel** para sincronizar múltiples hilos. Estos mecanismos de **alto  nivel** son los siguientes:

- [`Semaphore`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html): Esta clase implementa el concepto de semáforo, es un **TDA** que contiene una variable para controlar el acceso a recursos compartidos (en teoría se desarrollará con más detalle este concepto). Es uno de los elementos que tradicionalmente se han venido utilizando para la programación concurrente.

- [`Exchanger`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Exchanger.html): Esta clase define un mecanismo que proporciona un elemento para el intercambio de datos entre dos hilos.

La clase `Semaphore` proporciona un mecanismo genérico, para solucionar los problemas que se presentan en la programación concurrente, a diferencia del resto de clases que se han presentado. El resto de clases que se han presentado están diseñadas específicamente para el mecanismo que se ha descrito de ellas. Se presentarán diferentes ejemplo para ver cómo podemos utilizar cada una de estas clases:

4. [Controlando el acceso concurrente a un recurso.](https://gitlab.com/ssccdd/materialadicional/-/blob/master/Ejemplo4.md)
5. [Controlando el acceso concurrente a múltiples copias de un recurso.](https://gitlab.com/ssccdd/materialadicional/-/blob/master/Ejemplo5.md)
6. [Intercambiando información entre tareas concurrentes.](https://gitlab.com/ssccdd/materialadicional/-/blob/master/Ejemplo6.md)

---
[^nota1]: El guión se extrae del libro *Java 7 Concurrency Cookbook* que se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTY2MDI1ODgwNCw0MTIzODQ5ODIsLTgxNT
UwNTE5NV19
-->