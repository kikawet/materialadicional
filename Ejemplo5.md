# Controlando el acceso concurrente a múltiples copias de un recurso

En el [ejemplo anterior](https://gitlab.com/ssccdd/materialadicional/-/blob/master/Ejemplo4.md) hemos utilizado un **semáforo binario** para proteger el acceso a un recurso compartido como es una impresora. Ahora en este ejemplo utilizaremos el **semáforo** para garantizar el acceso a un número de impresoras que definen nuestro recurso compartido. Para ello modificaremos el ejemplo presentado anteriormente.

1. Modificaremos la clase `PrintQueue` para añadir dos variables de instancia, una que representará el número de impresoras que disponemos y la segunda para proteger el acceso a la variable que representa a las copias de las impresoras. El constructor deberá inicializar estas variables de instancia. Cuando creamos el semáforo inicializamos la variable que controla a 3, que será el número de instancias del recurso que disponemos. En nuestro ejemplo las impresoras.

```java
...
/**
 * Semaphore to control the access to the printers
 */
private Semaphore semaphore;
	
/**
 * Array to control what printer is free
 */
private boolean freePrinters[];
	
/**
 * Lock to control the access to the freePrinters array
 */
private Lock lockPrinters;
	
/**
 * Constructor of the class. It initializes the three objects
 */
public PrintQueue(){
    semaphore=new Semaphore(3);
    freePrinters=new boolean[3];
        for (int i=0; i<3; i++){
            freePrinters[i]=true;
        }
    lockPrinters=new ReentrantLock();
}
...
```

2. El siguiente paso será modificar el método `printJob()` dado que ahora disponemos de más de una impresora donde podremos enviar el trabajo de impresión. Por tanto, una vez que garantizamos el acceso al recurso mediante el **semáforo**, tendremos que localizar la impresora que esté libre antes de las sentencias que simularán la impresión. Para finalizar deberá indicarse que la impresora utilizada ya está disponible y liberar con seguridad el **semáforo** para evitar **deadlock**.

```java
...
public void printJob (Object document){
    try {
        // Get access to the semaphore. If there is one or more printers free,
        // it will get the access to one of the printers
        semaphore.acquire();
			
        // Get the number of the free printer
        int assignedPrinter=getPrinter();
			
        Long duration=(long)(Math.random()*10);
        System.out.printf("%s: PrintQueue: Printing a Job in Printer %d during %d seconds\n",Thread.currentThread().getName(),assignedPrinter,duration);
        TimeUnit.SECONDS.sleep(duration);
			
        // Free the printer
        freePrinters[assignedPrinter]=true;
    } catch (InterruptedException e) {
        e.printStackTrace();
    } finally {
        // Free the semaphore
        semaphore.release();
    }
}
...
```

3. Para localizar la impresora que está disponible definimos un método llamado `getPrinter()`. En este método primero utilizaremos una instancia de la clase `ReentrantLock`, como ya se ha visto en la [sesión 3](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio1.md), que garantiza el acceso seguro al array que representa a las impresoras que disponemos. Recorre el array y devolverá el índice de la primera impresora disponible.

```java
...
private int getPrinter() {
    int ret=-1;
		
    try {
        // Get the access to the array
        lockPrinters.lock();
        // Look for the first free printer
        for (int i=0; i<freePrinters.length; i++) {
            if (freePrinters[i]){
                ret=i;
                freePrinters[i]=false;
                break;
            }
        }
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        // Free the access to the array
        lockPrinters.unlock();
    }
    return ret;
}
...
```

4. El programa principal y la clase `Job` no sufrirán ningún cambio y se utilizan tal cual están en el ejemplo anterior.

## Información adicional

 En el ejemplo hemos inicializado el **semáforo** a 3 porque ese es el número de impresoras que hemos utilizado en nuestro ejemplo. Cuando se invoca el método `acquire()` el hilo no se bloqueará si el valor de la variable del **semáforo** no es `0`, por tanto disminuye el valor de la variable del **semáforo** en `1` y tenemos la seguridad que habrá disponible una impresora para poder completar el trabajo de impresión.

## Ejercicios propuestos

- ¿Por qué hay que utilizar una instancia de la clase `ReentrantLock` para garantizar el acceso a la variable de instancia que representa las impresoras que tenemos?
- Modificar el ejemplo para que sólo utilicemos la clase `Semaphore` para la resolución del ejemplo.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTcwMjQ4OTI2NV19
-->