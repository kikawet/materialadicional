# Usando condiciones en un código sincronizado

Uno de los problemas clásicos en la programación concurrentes es el **Productor/Consumidor**. Tenemos un espacio de memoria compartida donde uno o más productores almacenan datos y uno o más consumidores toman esos datos. Se tiene que garantizar el acceso concurrente al espacio de memoria por medio de las **secciones críticas** pero además hay otras limitaciones. Los productores no podrán almacenar datos si no tienen hueco en el espacio de memoria y los consumidores no podrán tomar datos si no hay ninguno en el espacio de memoria.

Java nos proporciona una serie de métodos de la clase [`Object`](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html) que permiten solucionar el problema:

- `wait()`: Suspenderá la ejecución del hilo y se liberará el bloqueo de la **sección crítica** controlada por el objeto para que otro hilo pueda ejecutarla. La ejecución de la `sección crítica` comenzará desde el inicio de la misma, independientemente desde donde se suspendió la ejecución del hilo.

- `notify()`: Reanudará la ejecución de un hilo que previamente quedó suspendido por una operación `wait()` para una **sección crítica** controlada por el objeto.
    
- `notifyAll()`: Reanudará la ejecución de todos los hilos que previamente quedaron suspendidos por una operación `wait()` para una **sección crítica** controlada por el objeto.
    
Presentaremos un problema de **Productor/Consumidor** para ejemplificar el uso de los métodos presentados anteriormente.

1. Definimos una clase llamada `EventStorage`. Tiene dos variables de instancia: una que representará el tamaño máximo y otra que representa una lista de fechas. El constructor dará un valor para estas dos variables de instancia.

```java
...
/**
 * Maximum size of the storage
 */
private int maxSize;
/**
 * Storage of events
 */
private List<Date> storage;
	
/**
 * Constructor of the class. Initializes the attributes.
 */
public EventStorage(){
    maxSize=10;
    storage=new LinkedList<>();
}
...
```

2. Implementaremos un método sincronizado `set()` que almacenará la fecha en la lista si no ha alcanzado el máximo, en otro caso se suspenderá. Una vez almacenada la fecha se mostrará por la consola el número de datos almacenados y liberará un hilo bloqueado previamente.

```java
...
/**
 * This method creates and storage an event.
 */
public synchronized void set(){
    while (storage.size()==maxSize){
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
	    }
	}
    storage.add(new Date());
    System.out.printf("Set: %d\n",storage.size());
    notify();
}
...
```

3. Implementamos un método sincronizado `get()`  que retirará una fecha de la lista si hay disponibles, en otro caso se suspenderá. Se presentará por la consola el número de fecha almacenado y la fecha almacenada. Para finalizar liberará un hilo bloqueado previamente.

```java
...
/**
 * This method delete the first event of the storage.
 */
public synchronized void get(){
    while (storage.size()==0){
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    System.out.printf("Get: %d: %s\n",storage.size(),((LinkedList<?>)storage).poll());
    notify();
}
...
```

4. Ahora definimos una clase llamada `Producer` que implementa la interface `Runnable`. Utilizaremos esta clase para representar el **productor**, tendrá como variable de instancia el objeto donde almacenaremos las fechas producidas. El constructor asociará el objeto donde almacenaremos las fechas. El método `run()` almacenará 100 fechas.

```java
/**
 * This class implements a producer of events.
 *
 */
public class Producer implements Runnable {

    /**
     * Store to work with
     */
    private EventStorage storage;
	
    /**
     * Constructor of the class. Initialize the storage.
     * @param storage The store to work with
     */
    public Producer(EventStorage storage){
        this.storage=storage;
    }
	
    /**
     * Core method of the producer. Generates 100 events.
     */
    @Override
    public void run() {
        for (int i=0; i<100; i++){
            storage.set();
        }
    }
}
```

5. Definimos una clase llamada `Consumer` que implementa la interface `Runnable`. Utilizaremos esta clase para representar el **consumidor**, tendrá como variable de instancia donde están almacenadas las fechas. El método `run()` retirará 100 fechas. La implementación es similar al caso del productor, revisad el código en el ejemplo.

6. Implementaremos el método `main(.)` donde se creará un objeto de la clase `EventStorage`, un objeto de la clase `Producer` y otro de la clase `Consumer`. Para el productor y el consumidor deberemos pasar como parámetro en los constructores el objeto donde se almacenarán las fechas. Asociaremos un hilo para el productor y otro para el consumidor antes de ejecutarlos.

## Ejercicios propuestos

- Modificar el ejemplo para que haya múltiples productores y consumidores.
- ¿Qué problemas te has encontrado al utilizar múltiples productores y consumidores?


<!--stackedit_data:
eyJoaXN0b3J5IjpbODEyNzA1NjAsLTcyNDU1MjUyOF19
-->