# Controlando el acceso concurrente a un recurso

En este ejemplo se presenta el uso de la herramienta **semáforo** para la resolución del acceso compartido a un recurso. Un **semáforo** es un **TDA** que contiene una variable entera, que se toma un valor cuando se cree el **semáforo**, y dos operaciones:

- Una para incrementar el valor de la variable del **semáforo**, método `acquire()` en Java:
	- Si la variable tiene un valor de `0` liberará un hilo previamente bloqueado en el **semáforo** y no se incrementará el valor de la variable.
	- En otro caso se incrementa el valor en 1.

- Una para decrementar el valor de la variable del **semáforo**, método `release()` en Java:
	- Si la variable tiene un valor de `0` se bloqueará el hilo en el **semáforo** y no se disminuye el valor de la variable.
	- En otro caso disminuye el valor en `1`.
    
La principal característica que define al **semáforo** es que esta operaciones se realizan de forma **atómica**, es decir, se completan sin interrupción, sobre el mismo **semáforo**, garantizando así el valor para la variable del **semáforo**[^nota1].

En el ejemplo utilizaremos un caso particular para el **semáforo** que se conoce como **semáforo binario**, es decir, crearemos un **semáforo** cuya variable tiene el valor `1`. Este tipo de semáforo será necesario para garantizar el acceso a un recurso no compatible, es decir, sólo un hilo podrá tener acceso al recurso al mismo tiempo, **sección crítica**. En nuestro ejemplo utilizamos como recurso no compatible una cola de impresión.

1. Definimos una clase llamada `PrintQueue` para implementar la cola de impresión. Tendrá un objeto de la clase [`Semaphore`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html), clase que implementa el **semáforo** en Java, como variable de instancia. Definimos el constructor que creará el **semáforo binario**.

```java
...
/**
 * Semaphore to control the access to the queue
 */
private final Semaphore semaphore;
	
/**
 * Constructor of the class. Initializes the semaphore
 */
public PrintQueue(){
    semaphore=new Semaphore(1);
}
...
```

2. El método que crearemos para realizar la tarea de impresión lo primero que hará es que invocar el método `acquiere()`  del **semáforo** para garantizar que otro hilo no ha realizado ya esta operación. Si no es el primero se bloqueará. Esta operación puede ser interrumpida generando una excepción de la clase [`InterruptedException`](https://docs.oracle.com/javase/8/docs/api/java/lang/InterruptedException.html).

3. Realizaremos una serie de operaciones para simular el tiempo que lleva realizar las operaciones de impresión.

4. Para finalizar invocamos el método `release()`  del **semáforo** en la cláusula `finally` que garantiza que se liberará el recurso para que otro hilo pueda acceder a él o liberará a uno previamente bloqueado en el acceso. Este es un punto crítico porque de no realizarlo de esta forma podría provocar un **deadlock**.

```java
...
/**
 * Method that simulates printing a document
 * @param document Document to print
 */
public void printJob (Object document){
    try {
        // Get the access to the semaphore. If other job is printing, this
        // thread sleep until get the access to the semaphore
        semaphore.acquire();
			
        Long duration=(long)(Math.random()*10);
        System.out.printf("%s: PrintQueue: Printing a Job during %d seconds\n",Thread.currentThread().getName(),duration);
        Thread.sleep(duration);
        TimeUnit.SECONDS.sleep(duration);
    } catch (InterruptedException e) {
        e.printStackTrace();
    } finally {
        // Free the semaphore. If there are other threads waiting for this semaphore,
        // the JVM selects one of this threads and give it the access.
        semaphore.release();
    }
}
...
```

5. Para acceder al recurso definimos una clase llamada `Job` que implementa la interface `Runnable`. Deberá tener como variable de instancia el recurso compartido al que deberá acceder. En nuestro caso la cola de impresión. La tarea que debe realizar, que se codifica en el método `run()`, es invocar el método `printJob(.)` del recurso compartido para simular el trabajo de impresión. En el ejemplo se puede ver el código de esta clase.

6. Por último debemos implementar el método `main(.)`  de la aplicación. Necesitamos crear un objeto que represente el recurso compartido para simular los trabajos de impresión. Creamos 10 hilos con 10 instancias de la clase `Job` que nos permitirá simular 10 tareas de impresión concurrentes. Para finalizar el método `main(.)` lanzaremos la ejecución de los hilos. El código de la clase principal se puede revisar en el ejemplo.

## Información adicional

La clase `Semaphore` tiene dos versiones adicionales para el método `adquire()`:

- `acquireUninterruptibly()`: En el método `adquire()`; cuando la variable del semáforo es `0`, bloquea al hilo hasta que se invoque el método `release()`. Mientras el hilo se encuentra bloqueado, puede solicitarse la interrupción de su ejecución y el método `adquire()` lanza una excepción de la clase `InterruptedException`. Con esta versión se ignoran las solicitudes de interrupción de la ejecución del hilo y no se lanza ninguna excepción.
- `tryAcquire()`: Este método se prueba el acceso sobre el **semáforo**, es decir, si es seguro acceder al recurso compartido. En ese caso devolverá el valor `true`, en caso contrario devuelve el valor `false` pero no se bloquea el hilo. Por tanto es responsabilidad del **programador** tomar las decisiones apropiadas para garantizar el correcto funcionamiento de la aplicación con los valores que devuelve el método.
    
## Ejercicios propuesto

- Revisar el [API de Java](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html) para comprobar los métodos disponibles para la clase `Semaphore` y su funcionamiento.
- Modificar el ejemplo número 1 de la sesión 2 utilizando la clase `Semaphore`.

---
[^nota1]: La herramienta **semáforo** se explicará en detalle en las clases de teoría.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIwODY2Njk3MV19
-->