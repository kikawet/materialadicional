# Sincronizando un método

En este ejemplo se presenta el uso de unos de los elementos de **sincronización** en Java más común, esto es, la utilización de la palabra reservada `synchronized` para controlar el acceso concurrente a un método. De esta forma, sólo un hilo podrá ejecutar un método declarado con la palabra reservada `synchronized` de ese objeto. Si otro hilo intenta ejecutar el mismo método de ese objeto, suspenderá su ejecución hasta que el hilo que lo está ejecutando termine. De esta forma, cualquier método que se declare con la palabra reservada `synchronized` será una **sección crítica**. Si un objeto tiene declarados varios métodos con la palabra reservada `synchronized` sólo se podrá ejecutar uno de ellos al mismo tiempo por un hilo (porque son mutuamente excluyentes)..

Los métodos de clase tienen un comportamiento diferente. Si un método de clase está declarado con la palabra reservada `synchronized` se permite a un hilo poder acceder a un método de instancia mientras otro hilo esté ejecutando ese método de clase. Es decir, podemos tener dos métodos declarados con la palabra reservada `synchronized`, uno de ellos de clase, y que se estén ejecutando concurrentemente. Esto es importante si ambos métodos acceden a recursos compartidos provocando errores de inconsistencia.

Para presentar este concepto, en el ejemplo se presentan dos hilos que acceden a un objeto común, disponemos de una cuenta bancaria y dos hilos; uno que ingresa dinero y otro que retira dinero de la cuenta. Si los métodos no están **sincronizados** veremos el error de inconsistencia que se produce. En los ejemplos disponibles están las dos posibilidades, **incorrecta** (no sincronizada) y la **correcta** (sincronizada). En la exposición del ejemplo se presenta la correcta.

1. Declaramos una clase con nombre `Account` que modela nuestra cuenta bancaria. Tendrá una variable de instancia para representar el estado de la cuenta. Además añadiremos los métodos de acceso a esa variable de instancia.

```java
...
/**
 * This class simulates a bank account 
 *
 */
public class Account {

    /**
     * Balance of the bank account
     */
    private double balance;

    /**
     * Returns the balance of the account
     * @return the balance of the account
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Establish the balance of the account
     * @param balance the new balance of the account
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }
...
```

2. Implementamos un método llamado `addAmount(.)` que nos permitirá incrementar el saldo de la cuenta bancaria. Este método sólo deberá ejecutarlo un único hilo para que el resultado sea correcto y no se pierda el ingreso en la cuenta. Para ello utilizaremos la palabra reservada `synchronized` para convertirlo en una **sección crítica**.

```java
...
/**
 * Add an import to the balance of the account
 * @param amount the import to add to the balance of the account
 */
public synchronized void addAmount(double amount) {
    double tmp=balance;
    try {
        Thread.sleep(10);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    tmp+=amount;
    balance=tmp;
}
...
```

3. Implementamos un método llamado `subtractAmount(.)` que nos permitirá disminuir el saldo de la cuenta bancaria. Este método sólo deberá ejecutarlo un único hilo para que el resultado sea correcto y no se pierda el ingreso en la cuenta. Para ello utilizaremos la palabra reservada `synchronized` para convertirlo en una **sección crítica**.

```java
...
/**
 * Subtract an import to the balance of the account
 * @param amount the import to subtract to the balance of the account 
 */
public synchronized void subtractAmount(double amount) {
    double tmp=balance;
    try {
        Thread.sleep(10);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    tmp-=amount;
    balance=tmp;
}
...
```

4. El siguiente paso será definir una clase que haga operaciones bancarias. Definimos una clase llamada `Bank` que implementa la interface `Runnable`. Añadimos una variable de instancia que almacene un objeto del tipo `Account` y definimos su constructor.

```java
...
/**
 * This class simulates a bank or a cash dispenser that takes money
 * from an account
 * 
 */
public class Bank implements Runnable {

    /**
     * The account affected by the operations
     */
    private Account account;
	
    /**
     * Constructor of the class. Initializes the account
     * @param account The account affected by the operations
     */
    public Bank(Account account) {
        this.account=account;
    }
...
```

5. Implementamos el método `run(.)` para que realice 100 operaciones para retirar dinero de la cuenta.

```java
...
/**
 * Core method of the Runnable
 */
public void run() {
    for (int i=0; i<100; i++){
        account.subtractAmount(1000);
    }
}
...
```

6. Ahora definimos una clase que simulará las operaciones de ingreso de un compañía. Llamamos a la clase `Company` e implementa la interface `Runnable`. Y como la clase anterior deberá tener una variable de instancia para almacenar la cuenta y crear su constructor. El código es similar y se puede consultar en el ejemplo.

7. Implementamos el método `run(.)` para que realice 100 operaciones de ingreso en la cuenta.

```java
...
/**
 * Core method of the Runnable
 */
public void run() {
    for (int i=0; i<100; i++){
        account.addAmount(1000);
    }
}
...
```

8. Implementamos el método `main(.)` de la aplicación. Tiene un objeto de la clase `Account` e inicializamos la cuenta. Luego en un objeto de la clase `Bank` asociamos la cuenta creada. Repetimos el proceso para un objeto de la clase `Company`. Asociamos los dos últimos objetos a dos de la clase `Thread` y mostramos por la consola el balance de la cuenta antes de iniciar los dos hilos.

```java
...
/**
 * Main method of the example
 * @param args
 */
public static void main(String[] args) {
    // Creates a new account ...
    Account	account=new Account();
    // an initialize its balance to 1000
    account.setBalance(1000);
		
    // Creates a new Company and a Thread to run its task
    Company	company=new Company(account);
    Thread companyThread=new Thread(company);
    // Creates a new Bank and a Thread to run its task
    Bank bank=new Bank(account);
    Thread bankThread=new Thread(bank);
		
    // Prints the initial balance
    System.out.printf("Account : Initial Balance: %f\n",account.getBalance());
		
    // Starts the Threads
    companyThread.start();
    bankThread.start();
...
```

9. Para finalizar utilizamos el método `join()` para esperar la finalización de los hilos antes de mostrar por la consola el balance de la cuenta.

```java
...
try {
    // Wait for the finalization of the Threads
    companyThread.join();
    bankThread.join();
    // Print the final balance
    System.out.printf("Account : Final Balance: %f\n",account.getBalance());
} catch (InterruptedException e) {
    e.printStackTrace();
}
...
```

10. Primero probaremos el ejemplo que no se está sincronizado antes de ejecutar el sincronizado. Comprobad si las soluciones difieren.

## Preguntas

- ¿Buscar la explicación para el resultado del ejemplo no sincronizado?
- ¿Por qué se han implementado los métodos de ingreso y retirada para que se suspenda su ejecución por un tiempo?
    
## Ejercicios propuestos

- Crear una clase que simule tanto ingresos como retiradas adaptando el método `main(.)` para comprobar su resultado.
- Modificar la clase Account para que sólo tengamos que sincronizar el mínimo de código necesario.
 

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTU4ODQxMDU5OSwtMjEzNTE0MjcxM119
-->